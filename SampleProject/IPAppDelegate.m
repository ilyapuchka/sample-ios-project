//
//  IPAppDelegate.m
//  SampleProject
//
//  Created by Ilya Puchka on 01.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import "IPAppDelegate.h"
#import <SDURLCache.h>

@implementation IPAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    UIColor *commonTintColor = [UIColor colorWithRed:106.0f/255.0f green:130.0f/255.0f blue:169.0f/255.0f alpha:1.0f];
    UIColor *tabBarTintColor = [UIColor colorWithRed:135.0f/255.0f green:159.0f/255.0f blue:197.0f/255.0f alpha:1.0f];

    if (![self.window respondsToSelector:@selector(setTintColor:)]) {
        //for ios6
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        [[UINavigationBar appearance] setTitleTextAttributes:@{
                                         UITextAttributeFont: [UIFont systemFontOfSize:20.0f],
                                    UITextAttributeTextColor: [UIColor darkTextColor],
                              UITextAttributeTextShadowColor: [UIColor clearColor]}];
        [[UIBarButtonItem appearance] setBackButtonBackgroundImage:[UIImage imageNamed:@"back_btn_icon_os6"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:@{
                                                                                UITextAttributeTextColor: commonTintColor,
                                                                                     UITextAttributeFont: [UIFont fontWithName:@"HelveticaNeue" size:18.0f],
                                                                          UITextAttributeTextShadowColor: [UIColor clearColor]}
                                                                                                forState:UIControlStateNormal];
        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:@{
                                                                                UITextAttributeTextColor: commonTintColor,
                                                                                     UITextAttributeFont: [UIFont fontWithName:@"HelveticaNeue" size:18.0f],
                                                                          UITextAttributeTextShadowColor: [UIColor clearColor]}
                                                                                                forState:UIControlStateHighlighted];
        
        [[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:@"tabbar_bg_os6"]];
        [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"tabbar_indicator_os6"]];
        [[UITabBar appearance] setSelectedImageTintColor:[UIColor whiteColor]];
        
        [[UITabBar appearance] setTintColor:tabBarTintColor];
        
    }
    else {
        [self.window setTintColor:commonTintColor];
//        [[UITabBar appearance] setBarTintColor:tabBarTintColor];
    }

    [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                        UITextAttributeFont:[UIFont fontWithName:@"Helvetica-Light" size:12.0f],
                                                        UITextAttributeTextColor:[UIColor colorWithRed:224.0f/255.0f
                                                                                                 green:229.0f/255.0f
                                                                                                  blue:239.0f/255.0f
                                                                                                 alpha:1.0f]}
                                             forState:UIControlStateNormal];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                        UITextAttributeFont:[UIFont fontWithName:@"Helvetica-Bold" size:12.0f],
                                                        UITextAttributeTextColor: [UIColor whiteColor]}
                                             forState:UIControlStateSelected];
    
    [[UISegmentedControl appearance] setDividerImage:[UIImage imageNamed:@"segmented_unsel_sel"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [[UISegmentedControl appearance] setDividerImage:[UIImage imageNamed:@"segmented_sel_unsel"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UISegmentedControl appearance] setBackgroundImage:[UIImage imageNamed:@"segmented_selected"] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [[UISegmentedControl appearance] setBackgroundImage:[UIImage imageNamed:@"segmented_normal"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UISegmentedControl appearance] setTitleTextAttributes:@{UITextAttributeFont: [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0f], UITextAttributeTextColor: [UIColor blackColor], UITextAttributeTextShadowColor: [UIColor clearColor]} forState:UIControlStateSelected];
    [[UISegmentedControl appearance] setTitleTextAttributes:@{UITextAttributeFont: [UIFont fontWithName:@"HelveticaNeue" size:14.0f], UITextAttributeTextColor: [UIColor colorWithRed:106.0f/255.0f green:130.0f/255.0f blue:169.0f/255.0f alpha:1.0f], UITextAttributeTextShadowColor: [UIColor clearColor]} forState:UIControlStateNormal];
    
    SDURLCache *urlCache = [[SDURLCache alloc] initWithMemoryCapacity:1024 * 1024
                                                         diskCapacity:20 * 1024 * 1024
                                                             diskPath:[SDURLCache defaultCachePath]];
    [NSURLCache setSharedURLCache:urlCache];

    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
