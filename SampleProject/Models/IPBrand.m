//
//  IPBrand.m
//  SampleProject
//
//  Created by Ilya Puchka on 01.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import "IPBrand.h"

@implementation IPBrand

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@>", self.name];
}

- (id)copyWithZone:(NSZone *)zone
{
    IPBrand *brand = [[IPBrand alloc] init];
    brand.name = self.name;
    brand.url = self.url;
    brand.imageURL = self.imageURL;
    brand.desc = self.desc;
    return brand;
}

@end
