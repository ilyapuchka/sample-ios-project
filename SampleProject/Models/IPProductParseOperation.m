//
//  IPProductParseOperation.m
//  SampleProject
//
//  Created by Ilya Puchka on 01.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import "IPProductParseOperation.h"
#import "IPProduct.h"
#import "IPPrice.h"
#import "IPBrand.h"

static NSString * const kProductElementName = @"product";
static NSString * const kTitleElementName = @"title";
static NSString * const kPriceElementName = @"price";
static NSString * const kBrandElementName = @"brand";
static NSString * const kBrandNameElementName = @"name";
static NSString * const kDescriptionElementName = @"description";
static NSString * const kImageElementName = @"image";
static NSString * const kMediaElementName = @"media";
static NSString * const kColorsElementName = @"colors";
static NSString * const kSizesElementName = @"sizes";
static NSString * const kSizeElementName = @"size";
static NSString * const kAttributesElementName = @"attributes";
static NSString * const kAttributesItemName = @"item";

@interface IPProductParseOperation() <NSXMLParserDelegate>

@property (nonatomic, copy) NSData *data;
@property (nonatomic, strong) NSXMLParser *parser;
@property (nonatomic, strong) IPProduct *currentProduct;
@property (nonatomic, strong) IPBrand *currentBrand;
@property (nonatomic, strong) IPPrice *currentPrice;
@property (nonatomic, strong) NSMutableString *currentParsedCharacters;
@property (nonatomic, strong) NSMutableArray *currentImagesArray;
@property (nonatomic, copy) IPProduct *result;
@property (nonatomic, strong) NSError *error;
@property (nonatomic) BOOL inColors;
@property (nonatomic, strong) NSMutableArray *currentSizes;
@property (nonatomic, strong) NSMutableArray *currentAvailableSizes;
@property (nonatomic, strong) NSMutableDictionary *currentAttributes;
@property (nonatomic, copy) NSString *currentAttributeKey;
@property (nonatomic, strong) IPProduct *currentOtherColorProduct;

@end

@implementation IPProductParseOperation

- (instancetype)initWithData:(NSData *)data
{
    self = [super init];
    if (self) {
        _data = data;
    }
    return self;
}

- (instancetype)initWithParser:(NSXMLParser *)parser
{
    self = [super init];
    if (self) {
        _parser = parser;
        _parser.delegate = self;
    }
    return self;
}

- (void)main
{
    NSLog(@"parse operation start");
    
    self.currentParsedCharacters = [@"" mutableCopy];
    self.inColors = NO;
    
    if (!self.parser && self.data) {
        self.parser = [[NSXMLParser alloc] initWithData:self.data];
        self.parser.delegate = self;
    }
    
    [self.parser parse];
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
    NSLog(@"document started");
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    NSLog(@"start element %@ namespace %@ qualifiedName %@, attributes %@", elementName, namespaceURI, qName, attributeDict);
    if ([elementName isEqualToString:kProductElementName]) {
        if (!self.inColors) {
            self.currentProduct = [[IPProduct alloc] init];
            self.currentProduct.sku = attributeDict[@"sku"];
            self.currentProduct.url = attributeDict[@"url"];
            self.currentProduct.sale = attributeDict[@"sale"];
        }
        else {
            self.currentOtherColorProduct = [[IPProduct alloc] init];
            if (attributeDict[@"image"]) {
                self.currentOtherColorProduct.images = @[attributeDict[@"image"]];
            }
            self.currentOtherColorProduct.sku = attributeDict[@"sku"];
            self.currentOtherColorProduct.url = attributeDict[@"url"];
        }
    }
    else if ([elementName isEqualToString:kColorsElementName]) {
        self.inColors = YES;
    }
    else if ([elementName isEqualToString:kTitleElementName] ||
             [elementName isEqualToString:kBrandNameElementName] ||
             [elementName isEqualToString:kDescriptionElementName]) {
        
        [self.currentParsedCharacters setString:@""];
    }
    else if ([elementName isEqualToString:kPriceElementName]) {
        self.currentPrice = [[IPPrice alloc] init];
        self.currentPrice.currency = attributeDict[@"currency"];
        self.currentPrice.discount = [attributeDict[@"discount_percent"] floatValue];
        self.currentPrice.originalPrice = [attributeDict[@"original_price"] floatValue];
        self.currentPrice.actualPrice = [attributeDict[@"value"] floatValue];
    }
    else if ([elementName isEqualToString:kBrandElementName]) {
        self.currentBrand = [[IPBrand alloc] init];
        self.currentBrand.imageURL = attributeDict[@"image"];
        self.currentBrand.url = attributeDict[@"url"];
    }
    else if ([elementName isEqualToString:kMediaElementName]) {
        self.currentImagesArray = [@[] mutableCopy];
    }
    else if ([elementName isEqualToString:kImageElementName]) {
        if (attributeDict[@"src"]) {
            [self.currentImagesArray addObject:attributeDict[@"src"]];
        }
    }
    else if ([elementName isEqualToString:kSizesElementName]) {
        self.currentSizes = [@[] mutableCopy];
        self.currentAvailableSizes = [@[] mutableCopy];
    }
    else if ([elementName isEqualToString:kSizeElementName]) {
        if (attributeDict[@"value"]) {
            [self.currentSizes addObject:attributeDict[@"value"]];
            if (!attributeDict[@"disabled"]) {
                [self.currentAvailableSizes addObject:attributeDict[@"value"]];
            }
        }
    }
    else if ([elementName isEqualToString:kAttributesElementName]) {
        self.currentAttributes = [@{} mutableCopy];
    }
    else if ([elementName isEqualToString:kAttributesItemName]) {
        self.currentAttributeKey = attributeDict[@"name"];
        [self.currentParsedCharacters setString:@""];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    NSLog(@"end element %@ namespace %@ qualifiedName %@", elementName, namespaceURI, qName);
    if ([elementName isEqualToString:kProductElementName]) {
        //return product
        if (!self.inColors) {
            self.result = self.currentProduct;
            self.currentProduct = nil;
            [parser abortParsing];
        }
        else if (self.currentOtherColorProduct) {
            if (!self.currentProduct.availableColors) {
                self.currentProduct.availableColors = @[];
            }
            self.currentProduct.availableColors = [self.currentProduct.availableColors arrayByAddingObject:self.currentOtherColorProduct];
            self.currentOtherColorProduct = nil;
        }
    }
    else if ([elementName isEqualToString:kTitleElementName]) {
        self.currentProduct.title = self.currentParsedCharacters;
    }
    else if ([elementName isEqualToString:kDescriptionElementName]) {
        if (self.currentBrand) {
            self.currentBrand.desc = self.currentParsedCharacters;
        }
        else {
            self.currentProduct.desc = self.currentParsedCharacters;
        }
    }
    else if ([elementName isEqualToString:kBrandNameElementName]) {
        self.currentBrand.name = self.currentParsedCharacters;
    }
    else if ([elementName isEqualToString:kBrandElementName]) {
        self.currentProduct.brand = self.currentBrand;
        self.currentBrand = nil;
    }
    else if ([elementName isEqualToString:kPriceElementName]) {
        self.currentProduct.price = self.currentPrice;
        self.currentPrice = nil;
    }
    else if ([elementName isEqualToString:kMediaElementName]) {
        self.currentProduct.images = self.currentImagesArray;
    }
    else if ([elementName isEqualToString:kColorsElementName]) {
        self.inColors = NO;
    }
    else if ([elementName isEqualToString:kSizesElementName]) {
        self.currentProduct.sizes = self.currentSizes;
        self.currentProduct.availableSizes = self.currentAvailableSizes;
    }
    else if ([elementName isEqualToString:kAttributesElementName]) {
        self.currentProduct.attributes = self.currentAttributes;
        self.currentAttributes = nil;
    }
    else if ([elementName isEqualToString:kAttributesItemName]) {
        if (self.currentAttributeKey.length && self.currentParsedCharacters.length) {
            [self.currentAttributes setObject:[self.currentParsedCharacters copy] forKey:self.currentAttributeKey];
        }
        self.currentAttributeKey = nil;
    }
}

- (void)parser:(NSXMLParser *)parser foundAttributeDeclarationWithName:(NSString *)attributeName forElement:(NSString *)elementName type:(NSString *)type defaultValue:(NSString *)defaultValue
{
    NSLog(@"found attribute with name %@ for element %@ type %@ default value %@", attributeName, elementName, type, defaultValue);
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (string.length) {
        NSLog(@"found characters %@", string);
        [self.currentParsedCharacters appendString:string];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    NSLog(@"document finished");
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    self.error = parseError;
    [parser abortParsing];
}

- (IPProduct *)product
{
    return self.result;
}

- (void)setCurrentProduct:(IPProduct *)currentProduct
{
    _currentProduct = currentProduct;
}

- (void)cancel
{
    NSLog(@"operation is canceled");
    [self.parser abortParsing];
    [super cancel];
}

@end
