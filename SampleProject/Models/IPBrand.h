//
//  IPBrand.h
//  SampleProject
//
//  Created by Ilya Puchka on 01.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IPBrand : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *imageURL;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *desc;

@end
