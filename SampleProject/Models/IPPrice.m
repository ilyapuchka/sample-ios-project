//
//  IPPrice.m
//  SampleProject
//
//  Created by Ilya Puchka on 01.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import "IPPrice.h"

@implementation IPPrice

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%.2f discount %.0f%% %.2f %@>", self.originalPrice, self.discount, self.actualPrice, self.currency];
}

- (id)copyWithZone:(NSZone *)zone
{
    IPPrice *price = [[IPPrice alloc] init];
    price.originalPrice = self.originalPrice;
    price.discount = self.discount;
    price.actualPrice = self.actualPrice;
    price.currency = self.currency;
    
    return price;
}

@end
