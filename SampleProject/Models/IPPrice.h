//
//  IPPrice.h
//  SampleProject
//
//  Created by Ilya Puchka on 01.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IPPrice : NSObject

@property (nonatomic, copy) NSString *currency;
@property (nonatomic) CGFloat discount;
@property (nonatomic) CGFloat originalPrice;
@property (nonatomic) CGFloat actualPrice;

@end
