//
//  IPProduct.m
//  SampleProject
//
//  Created by Ilya Puchka on 01.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import "IPProduct.h"
#import "IPBrand.h"

@implementation IPProduct

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@ - %@ %@", self.title, self.brand, self.price];
}

- (id)copyWithZone:(NSZone *)zone
{
    IPProduct *product = [[IPProduct alloc] init];
    product.price = self.price;
    product.brand = self.brand;
    product.title = self.title;
    product.images = self.images;
    product.sku = self.sku;
    product.url = self.url;
    product.sale = self.sale;
    product.categories = self.categories;
    product.defaultImageIndex = self.defaultImageIndex;
    product.attributes = self.attributes;
    product.desc = self.desc;
    product.sizes = self.sizes;
    product.availableSizes = self.availableSizes;
    product.availableColors = self.availableColors;
    product.rate = self.rate;
    product.reviews = self.reviews;
    product.compDesc = self.compDesc;
    return product;
}

- (void)didChangeValueForKey:(NSString *)key
{
    [super didChangeValueForKey:key];
    self.compDesc = nil;
}

- (NSAttributedString *)compDesc
{
    if (!_compDesc) {
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.lineSpacing = 4.f;
        style.paragraphSpacing = 6.0f;
        NSDictionary *attributes = @{NSParagraphStyleAttributeName: style};
        NSMutableAttributedString *compDesc = [[NSMutableAttributedString alloc] initWithString:self.desc
                                                                                     attributes: attributes];
        
        if (self.brand.name.length) {
            NSMutableAttributedString *brandTitleString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@: %@", NSLocalizedString(@"Brand", @"Brand"), self.brand.name]
                                                                                                 attributes:attributes];
            [brandTitleString addAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f]} range:[brandTitleString.string rangeOfString:NSLocalizedString(@"Brand", @"Brand")]];

            [compDesc appendAttributedString:brandTitleString];
        }

        for (NSString *attributeKey in self.attributes) {
            NSString *localizedAttributeKey = [NSLocalizedStringWithDefaultValue(attributeKey, nil, [NSBundle mainBundle], @" ", @"Product attribute") stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            NSString *attributeValue = [NSString stringWithFormat:@"%@", self.attributes[attributeKey]];
            if (localizedAttributeKey.length && attributeValue.length) {
                NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@: %@", localizedAttributeKey, attributeValue]
                                                                                                            attributes:attributes];
                [attributeString addAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f]}
                                         range:[attributeString.string rangeOfString:localizedAttributeKey]];
                
                [compDesc appendAttributedString:attributeString];
            }
        }
        _compDesc = [compDesc copy];
    }
    return _compDesc;
}

@end
