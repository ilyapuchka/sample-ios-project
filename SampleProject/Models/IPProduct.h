//
//  IPProduct.h
//  SampleProject
//
//  Created by Ilya Puchka on 01.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IPPrice, IPBrand;

@interface IPProduct : NSObject

@property (nonatomic, copy) NSString *sku;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *sale;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSArray *categories;
@property (nonatomic, copy) IPPrice *price;
@property (nonatomic, copy) IPBrand *brand;
@property (nonatomic, copy) NSArray *images;
@property (nonatomic) NSInteger defaultImageIndex;
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSDictionary *attributes;
@property (nonatomic, copy) NSAttributedString *compDesc;
@property (nonatomic, copy) NSArray *availableColors;
@property (nonatomic, copy) NSArray *sizes;
@property (nonatomic, copy) NSArray *availableSizes;
@property (nonatomic) CGFloat rate;
@property (nonatomic, copy) NSArray *reviews;

@end
