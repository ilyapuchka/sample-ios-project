//
//  IPAppDelegate.h
//  SampleProject
//
//  Created by Ilya Puchka on 01.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
