//
//  IPProductSizeCell.h
//  Lamoda
//
//  Created by Илья Пучка on 01.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IPProductSizeCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *sizeButton;

@end
