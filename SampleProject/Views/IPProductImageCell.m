//
//  IPProductImageCell.m
//  Lamoda
//
//  Created by Илья Пучка on 01.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import "IPProductImageCell.h"

@implementation IPProductImageCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)prepareForReuse
{
    self.imageView.image = nil;
}

@end
