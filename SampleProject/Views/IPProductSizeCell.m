//
//  IPProductSizeCell.m
//  Lamoda
//
//  Created by Илья Пучка on 01.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import "IPProductSizeCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation IPProductSizeCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.sizeButton.layer.cornerRadius = 2.0f;
    self.sizeButton.layer.masksToBounds = YES;
}

@end
