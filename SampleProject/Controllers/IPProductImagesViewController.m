//
//  IPProductImagesViewController.m
//  Lamoda
//
//  Created by Илья Пучка on 01.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import "IPProductImagesViewController.h"
#import "IPProductImageCell.h"
#import <UIImageView+AFNetworking.h>

@interface IPProductImagesViewController ()

@end

@implementation IPProductImagesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setImages:(NSArray *)images
{
    _images = [images copy];
    [self.pageControl setNumberOfPages:_images.count];
    [self.pageControl addTarget:self action:@selector(pageChanged:) forControlEvents:UIControlEventValueChanged];
    [self.collectionView reloadData];
}

- (void)setPageControl:(SMPageControl *)pageControl
{
    _pageControl = pageControl;
    [_pageControl setNumberOfPages:self.images.count];
    [_pageControl setIndicatorDiameter:8.f];
    [_pageControl setPageIndicatorTintColor:[UIColor colorWithRed:226.0f/255.0f
                                                            green:226.0f/255.0f
                                                             blue:226.0f/255.0f
                                                            alpha:1.0f]];
    
    [_pageControl setCurrentPageIndicatorTintColor:[UIColor colorWithRed:152.0f/255.0f
                                                                   green:152.0f/255.0f
                                                                    blue:152.0f/255.0f
                                                                   alpha:1.0f]];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.images.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    IPProductImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([IPProductImageCell class]) forIndexPath:indexPath];
    
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://pi0.lmcdn.ru/img320x461%@", self.images[indexPath.item]]];
    [cell.imageView setImageWithURL:imageURL];
    
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.pageControl updatePageNumberForScrollView:scrollView];
}

- (void)pageChanged:(SMPageControl *)pageControl
{
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:pageControl.currentPage inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
}

@end
