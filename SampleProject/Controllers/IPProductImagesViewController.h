//
//  IPProductImagesViewController.h
//  Lamoda
//
//  Created by Илья Пучка on 01.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SMPageControl.h>

@interface IPProductImagesViewController : UICollectionViewController

@property (nonatomic, weak) SMPageControl *pageControl;
@property (nonatomic, copy) NSArray *images;

@end
