//
//  IPProductViewController.m
//  SampleProject
//
//  Created by Ilya Puchka on 01.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import "IPProductViewController.h"
#import "Models.h"
#import <QuartzCore/QuartzCore.h>
#import "IPProductImagesViewController.h"
#import "IPProductSizeCell.h"
#import <DYRateView.h>
#import "IPProductImageCell.h"
#import <AFNetworking.h>
#import "Defines.h"

@interface IPProductViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UIScrollView *view;
@property (nonatomic, copy) IPProduct *product;

@property (weak, nonatomic) IBOutlet UILabel *discountLabel;
@property (weak, nonatomic) IBOutlet SMPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UIButton *addToCartButton;
@property (nonatomic, weak) IBOutlet UIView *sizesContainerView;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;

@property (weak, nonatomic) IPProductImagesViewController *productImagesViewController;
@property (weak, nonatomic) IBOutlet UICollectionView *sizesCollectionView;
@property (weak, nonatomic) IBOutlet UIView *reviewsView;
@property (weak, nonatomic) IBOutlet DYRateView *rateView;
@property (weak, nonatomic) IBOutlet UIButton *rateButton;
@property (weak, nonatomic) IBOutlet UILabel *reviewsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *descriptionSwitcher;
@property (weak, nonatomic) IBOutlet UICollectionView *colorsCollectionView;
@property (weak, nonatomic) IBOutlet UIView *colorsContainerView;
@property (nonatomic) NSInteger selectedSizeIndex;
@property (nonatomic, weak) CAGradientLayer *gradientLayer;

@end

@implementation IPProductViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.productImagesViewController = self.childViewControllers[0];
    
    UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareButton addTarget:self action:@selector(shareButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [shareButton setImage:[UIImage imageNamed:@"share_btn_icon"] forState:UIControlStateNormal];
    shareButton.bounds = CGRectMake(0, 0, 40, 33);
    UIBarButtonItem *shareButtonItem = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
    
    UIButton *likeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [likeButton addTarget:self action:@selector(likeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [likeButton setImage:[UIImage imageNamed:@"like_btn_icon"] forState:UIControlStateNormal];
    likeButton.bounds = CGRectMake(0, 0, 40, 33);
    UIBarButtonItem *likeButtonItem = [[UIBarButtonItem alloc] initWithCustomView:likeButton];
    
    [self.navigationItem setRightBarButtonItems:@[likeButtonItem, shareButtonItem]];
    
    self.discountLabel.layer.cornerRadius = 10.0f;
    self.discountLabel.layer.masksToBounds = YES;
    
    self.sizesContainerView.layer.cornerRadius = 2.0f;
    self.colorsContainerView.layer.cornerRadius = 2.0f;
    self.addToCartButton.layer.cornerRadius = 2.0f;
    
    self.rateView.fullStarImage = [UIImage imageNamed:@"star_selected_icon"];
    self.rateView.emptyStarImage = [UIImage imageNamed:@"star_normal_icon"];
    self.reviewsView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.reviewsView.layer.borderWidth = 1.0f;
    
    self.descriptionSwitcher.selectedSegmentIndex = 0;

    [self.addToCartButton setTitle:NSLocalizedString(@"Choose size", @"Choose size") forState:UIControlStateDisabled];
    [self.addToCartButton setTitle:NSLocalizedString(@"Add to cart", @"Add to cart") forState:UIControlStateNormal];
    
    self.selectedSizeIndex = -1;
}

- (void)configureWithProduct:(IPProduct *)product
{
    self.product = product;
    
    if (self.isViewLoaded) {
        self.title = self.product.brand.name;
        self.productImagesViewController.pageControl = self.pageControl;
        self.productImagesViewController.images = self.product.images;
        self.priceLabel.text = [NSString stringWithFormat:@"%.0f %@", self.product.price.actualPrice, NSLocalizedString(self.product.price.currency, @"Currency")];
        self.discountLabel.text = [NSString stringWithFormat:@"-%.0f%%", self.product.price.discount];
        if (self.product.reviews.count == 0) {
            self.rateView.hidden = YES;
            self.rateButton.hidden = NO;
            [self.rateButton setTitle:@"Написать отзыв" forState:UIControlStateNormal];
            self.reviewsCountLabel.hidden = YES;
        }
        else {
            self.rateView.rate = self.product.rate;
            self.rateView.hidden = NO;
            self.reviewsCountLabel.hidden = NO;
            self.reviewsCountLabel.text = [NSString stringWithFormat:@"%i", self.product.reviews.count];
            [self.rateButton setTitle:NSLocalizedString(@"Reviews", @"Reviews") forState:UIControlStateNormal];
        }
        
        [self descriptionSwitched:self.descriptionSwitcher];

        [self.sizesCollectionView reloadData];
        
        if (self.product.availableColors.count == 0) {
            [self.colorsContainerView removeFromSuperview];
        }
        else {
            [self.colorsCollectionView reloadData];
        }
        
        if (self.selectedSizeIndex != -1) {
            self.addToCartButton.enabled = YES;
        }
        else {
            self.addToCartButton.enabled = NO;
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self configureWithProduct:self.product];

    self.view.contentOffset = CGPointZero;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    self.view.contentOffset = CGPointZero;

    [super viewDidDisappear:animated];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if (!self.gradientLayer) {
        CAGradientLayer *gradientLayer = [[CAGradientLayer alloc] init];
        gradientLayer.zPosition = -100;
        gradientLayer.colors = @[(id)[UIColor whiteColor].CGColor, (id)[UIColor lightGrayColor].CGColor];
        gradientLayer.locations = @[@0.0f, @1.0f];
        [self.view.layer addSublayer:gradientLayer];
        self.gradientLayer = gradientLayer;
    }
    CGRect frame = CGRectZero;
    frame.origin.y = self.productImagesViewController.collectionView.superview.frame.size.height - self.productImagesViewController.collectionView.superview.frame.origin.y;
    frame.size.width = self.view.bounds.size.width;
    frame.size.height = self.reviewsView.superview.frame.origin.y - frame.origin.y;
    self.gradientLayer.frame = frame;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)shareButtonTapped:(id)sender
{
    
}

- (void)likeButtonTapped:(id)sender
{
    
}

- (IBAction)addToCartButtonTapped:(id)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:self.pageControl.currentPage inSection:0];
    if (indexPath.item < self.product.images.count) {
        IPProductImageCell *cell = (IPProductImageCell *)[self.productImagesViewController.collectionView cellForItemAtIndexPath:indexPath];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:cell.imageView.image];
        imageView.frame = [self.productImagesViewController.collectionView convertRect:cell.frame toView:self.tabBarController.view];
        [self.tabBarController.view addSubview:imageView];
        [UIView animateWithDuration:0.5f animations:^{
            imageView.center = CGPointMake(self.tabBarController.tabBar.bounds.size.width * 3/4, self.tabBarController.tabBar.center.y);
            CGAffineTransform transform = CGAffineTransformMakeScale(0.1, 0.1);
            transform = CGAffineTransformConcat(transform, CGAffineTransformRotate(imageView.transform, M_PI_2));
            imageView.transform = transform;
            imageView.alpha = 0.75f;
        } completion:^(BOOL finished) {
            [imageView removeFromSuperview];
            [[NSNotificationCenter defaultCenter] postNotificationName:IPAddToCartNotificationName object:nil userInfo:@{@"product": self.product}];
        }];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.sizesCollectionView) {
        IPProductSizeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([IPProductSizeCell class]) forIndexPath:indexPath];
        [cell.sizeButton setTitle:self.product.sizes[indexPath.item] forState:UIControlStateNormal];
        if ([self.product.availableSizes containsObject:self.product.sizes[indexPath.item]]) {
            [cell.sizeButton setTitleColor:[UIColor colorWithRed:106.0f/255.0f green:130.0f/255.0f blue:169.0f/255.0f alpha:1.0f]forState:UIControlStateNormal];
            cell.userInteractionEnabled = YES;
        }
        else {
            [cell.sizeButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            cell.userInteractionEnabled = NO;
        }
        
        return cell;
    }
    else {
        IPProductImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([IPProductImageCell class]) forIndexPath:indexPath];
        NSString *imageURL = [NSString stringWithFormat:@"http://pi0.lmcdn.ru/img320x461%@", [[(IPProduct *)self.product.availableColors[indexPath.item] images] lastObject]];
        [cell.imageView setImageWithURL:[NSURL URLWithString:imageURL]];
        
        return cell;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.sizesCollectionView) {
        return self.product.sizes.count;
    }
    else {
        return self.product.availableColors.count;
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    CGSize cellSize = [(UICollectionViewFlowLayout *)[collectionView collectionViewLayout] itemSize];

    CGFloat padding = 0;
    if (collectionView == self.sizesCollectionView) {
        padding = MAX(5, (collectionView.bounds.size.width - self.product.sizes.count * cellSize.width)/2);
    }
    else {
        padding = MAX(5, (collectionView.bounds.size.width - self.product.availableColors.count * cellSize.width)/2);
    }
    
    collectionView.scrollEnabled = (padding == 5);
    
    return UIEdgeInsetsMake(0, padding, 0, padding);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.sizesCollectionView) {
        IPProductSizeCell *cell = (IPProductSizeCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [cell.sizeButton setBackgroundColor:[UIColor colorWithRed:1.0f green:158.0f/255.0f blue:52.0f/255.0f alpha:1.0f]];
        [cell.sizeButton setSelected:YES];
        self.addToCartButton.enabled = YES;
        self.selectedSizeIndex = indexPath.item;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.sizesCollectionView) {
        IPProductSizeCell *cell = (IPProductSizeCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [cell.sizeButton setBackgroundColor:[UIColor clearColor]];
        [cell.sizeButton setSelected:NO];
    }
}

- (IBAction)reviewsButtonTapped:(id)sender
{
}

- (IBAction)descriptionSwitched:(UISegmentedControl *)sender
{
    if (sender.selectedSegmentIndex == 0) {
        self.descriptionLabel.attributedText = self.product.compDesc;
    }
    else {
        self.descriptionLabel.text = self.product.brand.desc;
    }
}

@end
