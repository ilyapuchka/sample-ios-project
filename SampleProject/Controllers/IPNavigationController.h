//
//  IPNavigationController.h
//  SampleProject
//
//  Created by Ilya Puchka on 02.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IPNavigationController : UINavigationController

@end
