//
//  IPTabBarController.m
//  SampleProject
//
//  Created by Ilya Puchka on 01.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import "IPTabBarController.h"

@interface IPTabBarController ()

@end

@implementation IPTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.viewControllers[0] setTitle:NSLocalizedString(@"Catalogue", @"Catalogue")];
    [[self.viewControllers[0] tabBarItem] setFinishedSelectedImage:[UIImage imageNamed:@"catalog_tabbar_selected_icon"] withFinishedUnselectedImage:[UIImage imageNamed:@"catalog_tabbar_icon"]];
    [self.viewControllers[1] setTitle:NSLocalizedString(@"Cart", @"Cart")];
    [[self.viewControllers[1] tabBarItem] setFinishedSelectedImage:[UIImage imageNamed:@"cart_tabbar_selected_icon"] withFinishedUnselectedImage:[UIImage imageNamed:@"cart_tabbar_icon"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
