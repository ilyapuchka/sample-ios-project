//
//  IPCatalogViewController.m
//  SampleProject
//
//  Created by Ilya Puchka on 01.08.13.
//  Copyright (c) 2013 Ilya Puchka. All rights reserved.
//

#import "IPCatalogViewController.h"
#import "Models.h"
#import "IPProductViewController.h"
#import <AFNetworking.h>
#import <MBProgressHUD.h>

@interface IPCatalogViewController () <NSURLConnectionDataDelegate, UITextFieldDelegate>

@property (nonatomic, strong) NSOperationQueue *httpQueue;
@property (nonatomic, strong) NSOperationQueue *parseQueue;
@property (nonatomic, strong) __block NSOperation *currentParseOperation;
@property (nonatomic, strong) NSArray *urls;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) NSURLConnection *urlConnection;
@property (nonatomic) NSMutableData *loadedData;
@property (nonatomic, strong) AFHTTPClient *httpClient;

@end

@implementation IPCatalogViewController

- (AFHTTPClient *)httpClient
{
    if (!_httpClient) {
        _httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:@"http://xml-test.herokuapp.com"]];
        [_httpClient registerHTTPOperationClass:[AFXMLRequestOperation class]];
    }
    return _httpClient;
}

- (UIActivityIndicatorView *)activityIndicator
{
    if (!_activityIndicator) {
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
        _activityIndicator.center = CGPointMake(CGRectGetMidX(keyWindow.bounds), CGRectGetMidY(keyWindow.bounds));
        [keyWindow addSubview:_activityIndicator];
    }
    return _activityIndicator;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    self.title = NSLocalizedString(@"Catalogue", @"Catalogue");
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.urls = @[[NSURL URLWithString:@"http://xml-test.herokuapp.com/product.xml"],
                  [NSURL URLWithString:@"http://xml-test.herokuapp.com/product2.xml"]];
    
    self.parseQueue = [NSOperationQueue new];
    self.httpQueue = [NSOperationQueue new];
}

- (void)handleError:(NSError *)error
{
    NSString *errorMessage = [error localizedDescription];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:errorMessage delegate:nil cancelButtonTitle:NSLocalizedString(@"Close", @"Close") otherButtonTitles:nil];
    [alertView show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.urls.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.row < self.urls.count) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        cell.textLabel.text = [self.urls[indexPath.row] absoluteString];
    }
    else if (indexPath.row == self.urls.count) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"InputCell" forIndexPath:indexPath];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.urls.count) {
        NSLog(@"currentParseOperation %@", self.currentParseOperation);
        if ([self.currentParseOperation isExecuting]) {
            [self.currentParseOperation cancel];
        }
        
        NSURL *url = self.urls[indexPath.row];
        [self getDataFromURL:url];
    }
}

- (void)getDataFromURL:(NSURL *)url
{
    static AFXMLRequestOperation *op = nil;
    if ([op isExecuting]) {
        NSLog(@"cancel url operation");
        [op cancel];
    }
    op = [[AFXMLRequestOperation alloc] initWithRequest:[NSURLRequest requestWithURL:url]];
    
    __block MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[[UIApplication sharedApplication] keyWindow] animated:YES];;
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.userInteractionEnabled = NO;
    
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (operation.response.statusCode == 200 && responseObject) {
            if ([self.currentParseOperation isExecuting]) {
                [self.currentParseOperation cancel];
            }
            
            IPProductParseOperation *parseOperation = [[IPProductParseOperation alloc] initWithParser:responseObject];
            self.currentParseOperation = parseOperation;
            
            __weak IPProductParseOperation *weakOp = parseOperation;
            parseOperation.completionBlock = ^(void){
                IPProductParseOperation *strongOp = weakOp;
                if (!strongOp.isCancelled) {
                    NSLog(@"parse operation finished");
                    if (strongOp.product) {
                        IPProductViewController *productViewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([IPProductViewController class])];
                        [productViewController configureWithProduct:strongOp.product];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [hud hide:YES];
                            hud = nil;

                            [self.navigationController pushViewController:productViewController animated:YES];
                        });
                    }
                    else if (strongOp.error) {
                        [self performSelectorOnMainThread:@selector(handleError:) withObject:strongOp.error waitUntilDone:NO];
                    }
                    self.currentParseOperation = nil;
                }
            };
            
            [self.parseQueue addOperation:self.currentParseOperation];
        }
        else {
            [hud hide:YES];
            hud = nil;
            
            NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:operation.response.statusCode userInfo:nil];
            [self performSelectorOnMainThread:@selector(handleError:) withObject:error waitUntilDone:NO];
        }
     
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [hud hide:YES];
        hud = nil;
        
        if (error.code != NSURLErrorCancelled) {
            [self performSelectorOnMainThread:@selector(handleError:) withObject:error waitUntilDone:NO];
        }
    }];
    
    [self.httpClient enqueueHTTPRequestOperation:op];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField endEditing:YES];
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([self validateUrl:textField.text]) {
        NSURL *url = [NSURL URLWithString:textField.text];
        if (url) {
            self.urls = [self.urls arrayByAddingObject:url];
            [self.tableView reloadData];
        }
        textField.text = @"";
    }
}

- (BOOL) validateUrl: (NSString *) candidate {
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}

@end
